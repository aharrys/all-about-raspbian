Setting WPA2 Enterprise on your raspberrry pi

1. Setting IP static on dhcpcd configuration
sudo nano /etc/dhcpcd.conf
Seting interface wlan0 if your raspberry using WIFI.

2. Setting interface, this setting make your raspberry can't search available wifi on your desktop
sudo nano /etc/network/interfaces
Just copy paste and change IP address, gateway & domain server.

3. change WPA supplicant configuration
sudo nano /etc/wpa_supplicant/wpa_supplicant.conf
this configuration setting your SSID, Identity, and password.

4. Make sure all config already done.

5. reboot your raspberry
sudo reboot

6. After reboot is finish, check IP address and Wifi.
check IP address :
ifconfig
check Wifi :
iwconfig


